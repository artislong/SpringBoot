package com.gnd.ws.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.client.core.WebServiceTemplate;

/**
 * Created by 陈敏 on 2017/8/21.
 */
@Configuration
public class WsClientConfiguration {

    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        return webServiceTemplate;
    }
}
