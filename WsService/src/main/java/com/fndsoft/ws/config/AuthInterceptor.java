package com.fndsoft.ws.config;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.saaj.SAAJInInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NodeList;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

/**
 * Created by 陈敏 on 2017/7/20.
 */
public class AuthInterceptor extends AbstractPhaseInterceptor<SoapMessage> {

    private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);
    private SAAJInInterceptor saajInInterceptor = new SAAJInInterceptor();

    private static final String USER_NAME = "admin";
    private static final String USER_PASSWORD = "123";

    public AuthInterceptor() {
        super(Phase.PRE_PROTOCOL);
        getAfter().add(SAAJInInterceptor.class.getName());
    }

    @Override
    public void handleMessage(SoapMessage soapMessage) throws Fault {
        SOAPMessage message = soapMessage.getContent(SOAPMessage.class);
        if (message == null) {
            saajInInterceptor.handleMessage(soapMessage);
            message = soapMessage.getContent(SOAPMessage.class);
        }
        SOAPHeader header = null;
        try {
            header = message.getSOAPHeader();
        } catch (SOAPException e) {
            logger.error("getSOAPHeader error: {}", e.getMessage(), e);
        }
        if (header == null) {
            throw new Fault(new IllegalArgumentException("找不到Header，无法验证用户信息"));
        }

        NodeList users = header.getElementsByTagName("username");
        NodeList passwords = header.getElementsByTagName("password");
        if (users.getLength() < 1) {
            throw new Fault(new IllegalArgumentException("找不到用户信息"));
        }
        if (passwords.getLength() < 1) {
            throw new Fault(new IllegalArgumentException("找不到密码信息"));
        }
        String username = users.item(0).getTextContent().trim();
        String password = passwords.item(0).getTextContent().trim();
        if (USER_NAME.equals(username) && USER_PASSWORD.equals(password)) {
            logger.debug("admin auth success!");
        } else {
            SOAPException soapException = new SOAPException("认证失败");
            logger.debug("admin auth failed!");
            throw new Fault(soapException);
        }
    }
}
