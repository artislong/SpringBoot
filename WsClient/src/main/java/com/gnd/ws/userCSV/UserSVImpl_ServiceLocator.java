/**
 * UserSVImpl_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.gnd.ws.userCSV;

public class UserSVImpl_ServiceLocator extends org.apache.axis.client.Service implements com.gnd.ws.userCSV.UserSVImpl_Service {

    public UserSVImpl_ServiceLocator() {
    }


    public UserSVImpl_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public UserSVImpl_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for UserSVImpl
    private java.lang.String UserSVImpl_address = "http://localhost:8080/webservices/userCSV";

    public java.lang.String getUserSVImplAddress() {
        return UserSVImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String UserSVImplWSDDServiceName = "UserSVImpl";

    public java.lang.String getUserSVImplWSDDServiceName() {
        return UserSVImplWSDDServiceName;
    }

    public void setUserSVImplWSDDServiceName(java.lang.String name) {
        UserSVImplWSDDServiceName = name;
    }

    public com.gnd.ws.userCSV.UserSV getUserSVImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(UserSVImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getUserSVImpl(endpoint);
    }

    public com.gnd.ws.userCSV.UserSV getUserSVImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.gnd.ws.userCSV.UserSVImplSoapBindingStub _stub = new com.gnd.ws.userCSV.UserSVImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getUserSVImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setUserSVImplEndpointAddress(java.lang.String address) {
        UserSVImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.gnd.ws.userCSV.UserSV.class.isAssignableFrom(serviceEndpointInterface)) {
                com.gnd.ws.userCSV.UserSVImplSoapBindingStub _stub = new com.gnd.ws.userCSV.UserSVImplSoapBindingStub(new java.net.URL(UserSVImpl_address), this);
                _stub.setPortName(getUserSVImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("UserSVImpl".equals(inputPortName)) {
            return getUserSVImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.fndsoft.com/", "UserSVImpl");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.fndsoft.com/", "UserSVImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("UserSVImpl".equals(portName)) {
            setUserSVImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
