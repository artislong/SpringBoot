package com.gnd.quartz.task;

import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by on 2017/8/8.
 */
@Component
public class ScheduleTasks {
	
	/**
	 *  定时任务启动入口
	 */
    public void test() throws JobExecutionException {
        System.out.println(new Timestamp(new Date().getTime()));
    }
}
