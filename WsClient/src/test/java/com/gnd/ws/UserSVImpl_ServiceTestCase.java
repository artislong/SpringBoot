/**
 * UserSVImpl_ServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.gnd.ws;

public class UserSVImpl_ServiceTestCase extends junit.framework.TestCase {
    public UserSVImpl_ServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testUserSVImplWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.gnd.ws.userCSV.UserSVImpl_ServiceLocator().getUserSVImplAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.gnd.ws.userCSV.UserSVImpl_ServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1UserSVImplAddUser() throws Exception {
        com.gnd.ws.userCSV.UserSVImplSoapBindingStub binding;
        try {
            binding = (com.gnd.ws.userCSV.UserSVImplSoapBindingStub)
                          new com.gnd.ws.userCSV.UserSVImpl_ServiceLocator().getUserSVImpl();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.gnd.ws.userCSV.User value = null;
        value = binding.addUser(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

}
