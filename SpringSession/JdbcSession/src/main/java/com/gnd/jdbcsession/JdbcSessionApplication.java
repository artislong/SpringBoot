package com.gnd.jdbcsession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@SpringBootApplication
@EnableJdbcHttpSession
public class JdbcSessionApplication {
    public static void main(String[] args){
        SpringApplication.run(JdbcSessionApplication.class, args);
    }
}
