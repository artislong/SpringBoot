package com.fndsoft.ws.commons;

/**
 * Created by 陈敏 on 2017/3/21.
 * 此类用来定义全局的常量
 */
public class GlobleConstant {
    public static final String TARGET_NAMESPACE = "http://ws.fndsoft.com/";
}
