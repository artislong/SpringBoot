package com.fndsoft.ws.wsService.impl;

import com.fndsoft.ws.wsBean.User;
import com.fndsoft.ws.wsService.interfaces.IUserSV;
import org.springframework.stereotype.Service;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@Service
public class UserSVImpl implements IUserSV {
    @Override
    public User addUser(String username, String password) {
        return new User(username, password);
    }
}
