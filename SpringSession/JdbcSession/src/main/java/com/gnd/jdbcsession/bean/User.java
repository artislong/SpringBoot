package com.gnd.jdbcsession.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 973893179319654305L;
    private Long id;
    private String username;
    private String password;
}
