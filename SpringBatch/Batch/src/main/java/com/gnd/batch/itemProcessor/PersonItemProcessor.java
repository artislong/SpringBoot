package com.gnd.batch.itemProcessor;

import com.gnd.batch.bean.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class PersonItemProcessor implements ItemProcessor<Person, Person> {

    private static final String GET_PRODUCT = "SELECT * FROM PERSON WHERE NAME = ?";
    public static final Logger logger = LoggerFactory.getLogger(PersonItemProcessor.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Person process(Person person) throws Exception {
        List<Person> persons = jdbcTemplate.query(GET_PRODUCT, new Object[]{person.getName()}, new BeanPropertyRowMapper<>(Person.class));
        if (persons.size() > 0) {
            logger.info("该数据已录入！！！");
        }
        String sex = null;
        if ("0".equals(person.getSex())) {
            sex = "男";
        } else {
            sex = "女";
        }
        logger.info("转换(性别：" + person.getSex() + ")为(" + sex + ")");
        final Person transformedPerson = new Person(person.getName(), person.getAge(), sex);
        return transformedPerson;
    }
}
