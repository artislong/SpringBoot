package com.gnd.async.controller;

import com.gnd.async.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/hello")
@RestController
public class HelloController {

    @Autowired
    private AsyncService asyncService;

    @GetMapping("/getUser")
    public Object getUser(@RequestParam("userName") String userName) throws Exception {
        return asyncService.findUser(userName).get();
    }
}
