package com.fndsoft.ws.wsService.interfaces;

import com.fndsoft.ws.wsBean.User;

/**
 * Created by 陈敏 on 2017/7/20.
 */
public interface IUserSV {
    User addUser(String username, String password);
}
