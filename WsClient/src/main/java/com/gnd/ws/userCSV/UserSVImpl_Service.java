/**
 * UserSVImpl_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.gnd.ws.userCSV;

public interface UserSVImpl_Service extends javax.xml.rpc.Service {
    public java.lang.String getUserSVImplAddress();

    public com.gnd.ws.userCSV.UserSV getUserSVImpl() throws javax.xml.rpc.ServiceException;

    public com.gnd.ws.userCSV.UserSV getUserSVImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
