package com.fndsoft.ws.wsBean;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = -2246769510878902864L;

    public User() {
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    private String username;
    private String password;
}
