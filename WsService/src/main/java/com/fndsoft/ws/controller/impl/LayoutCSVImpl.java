package com.fndsoft.ws.controller.impl;

import com.fndsoft.ws.commons.GlobleConstant;
import com.fndsoft.ws.controller.interfaces.ILayoutCSV;
import com.fndsoft.ws.wsBean.OperationResult;
import com.fndsoft.ws.wsService.interfaces.Layout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@WebService(
        serviceName = "ILayoutCSV", portName = "ILayoutCSV",
        targetNamespace = GlobleConstant.TARGET_NAMESPACE,
        endpointInterface = "com.fndsoft.ws.controller.interfaces.ILayoutCSV"
)
@Service
public class LayoutCSVImpl implements ILayoutCSV{

    @Autowired
    private Layout layout;

    @Override
    public String sayHello(String name) {
        return layout.sayHello(name);
    }

    @Override
    public OperationResult addLayout(String layoutName, String layoutContent) {
        return layout.addLayout(layoutName, layoutContent);
    }
}
