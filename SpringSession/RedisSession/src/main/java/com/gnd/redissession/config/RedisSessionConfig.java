package com.gnd.redissession.config;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@EnableRedisHttpSession
public class RedisSessionConfig {
}
