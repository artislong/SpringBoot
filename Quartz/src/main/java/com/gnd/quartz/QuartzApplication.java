package com.gnd.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@SpringBootApplication
public class QuartzApplication {
    public static void main(String[] args){
        SpringApplication.run(QuartzApplication.class, args);
    }
}
