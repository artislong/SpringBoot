package com.gnd.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/6/5.
 */
@SpringBootApplication
public class RestApplication {

    public static void main(String[] args){
        SpringApplication.run(RestApplication.class, args);
    }
}
