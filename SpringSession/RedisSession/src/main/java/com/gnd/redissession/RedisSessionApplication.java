package com.gnd.redissession;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@SpringBootApplication
public class RedisSessionApplication {
    public static void main(String[] args){
        SpringApplication.run(RedisSessionApplication.class, args);
    }
}
