package com.fndsoft.ws.wsService.interfaces;

import com.fndsoft.ws.wsBean.OperationResult;

/**
 * Created by 陈敏 on 2017/7/20.
 */
public interface Layout {

    String sayHello(String name);

    OperationResult addLayout(String layoutName, String layoutContent);
}
