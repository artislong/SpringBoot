package com.gnd.redis.test.bean;

import java.util.UUID;

/**
 * 
  *<p>类描述：Entity顶级父类。</p>
  * @author 张锡梓
  * @version v1.0.0.1。
  * @since JDK1.8。
  *<p>创建日期：2017年7月24日 上午11:35:58。</p>
 */
public class BaseEntity {

	/**
	 * ID
	 */
	protected String oid = UUID.randomUUID().toString();
	
	/**
	 * 是否尚未持久化
	 */
	private Boolean isTransient = true;
	
	/**
	 * 对象操作状态
	 * -1 : 删除，
		0 : 不变，
		1 : 新增，
		2 : 修改
	 */
	private Integer operationFlag = 0;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public Boolean getIsTransient() {
		return isTransient;
	}
	
	public void setIsTransient(Boolean isTransient) {
		this.isTransient = isTransient;
	}

	public Integer getOperationFlag() {
		return operationFlag;
	}

	public void setOperationFlag(Integer operationFlag) {
		this.operationFlag = operationFlag;
	}
}
