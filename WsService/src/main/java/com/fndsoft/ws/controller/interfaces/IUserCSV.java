package com.fndsoft.ws.controller.interfaces;

import com.fndsoft.ws.commons.GlobleConstant;
import com.fndsoft.ws.wsBean.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@WebService(name = "IUserCSV", targetNamespace = GlobleConstant.TARGET_NAMESPACE)
public interface IUserCSV {
    @WebMethod
    User addUser(@WebParam(name = "username") String username,
                 @WebParam(name = "password") String password);
}
