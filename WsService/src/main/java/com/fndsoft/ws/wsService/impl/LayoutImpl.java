package com.fndsoft.ws.wsService.impl;

import com.fndsoft.ws.wsBean.OperationResult;
import com.fndsoft.ws.wsService.interfaces.Layout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@Service
public class LayoutImpl implements Layout{

    private static final Logger logger = LoggerFactory.getLogger(LayoutImpl.class);

    @Override
    public String sayHello(String name) {
        return "Hello, " + name;
    }

    @Override
    public OperationResult addLayout(String layoutName, String layoutContent) {
        logger.info("layoutName:{}, layoutContent:{}", layoutName, layoutContent);
        if(ObjectUtils.isEmpty(layoutContent) || ObjectUtils.isEmpty(layoutContent)) {
            return new OperationResult(false, "参数不能为空");
        }
        return new OperationResult(true, null);
    }
}
