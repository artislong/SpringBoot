package com.gnd.async;

import com.gnd.async.bean.User;
import com.gnd.async.service.AsyncService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CompletableFuture;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AsyncApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(AsyncApplicationTests.class);

    @Autowired
    private AsyncService asyncService;

    @Test
    public void test01() throws Exception {
        long start = System.currentTimeMillis();
        logger.info("111111111111111111111111111");
        CompletableFuture<User> user1 = asyncService.findUser("artislong");
        logger.info("--> " + user1.get());
        logger.info("222222222222222222222222222");
        CompletableFuture<User> user2 = asyncService.findUser("artislong");
        logger.info("--> " + user2.get());
        logger.info("333333333333333333333333333");
        CompletableFuture<User> user3 = asyncService.findUser("artislong");
        logger.info("--> " + user3.get());
        CompletableFuture.allOf(user1, user2, user3).join();
        logger.info("Elapsed time: " + (System.currentTimeMillis() - start));
        logger.info("--> " + user1.get());
        logger.info("--> " + user2.get());
        logger.info("--> " + user3.get());
    }
}
