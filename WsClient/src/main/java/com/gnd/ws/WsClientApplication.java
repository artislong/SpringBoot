package com.gnd.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/7/19.
 */
@SpringBootApplication
public class WsClientApplication {
    public static void main(String[] args){
        SpringApplication.run(WsClientApplication.class, args);
    }
}
