package com.gnd.redis.test.bean;

import java.io.Serializable;

public class Domain extends CommonEntity implements Serializable{

    private static final long serialVersionUID = -5809782278272943999L;

    private String type;//值域类型
    private String code;//值域编码
    private String name;//值域名称
    private String orderNum;//排序编码
    private String description;//值域描述

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Domain{" +
                "type='" + type + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", description='" + description + '\'' +
                ", oid='" + oid + '\'' +
                '}';
    }
}
