package com.gnd.redissession.controller;

import com.gnd.redissession.bean.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    HttpSession session;

    @RequestMapping("/login")
    public Object login(@RequestBody User user) {
        Map<String, Object> data = new HashMap<>();
        if("admin".equals(user.getUsername()) && "admin".equals(user.getPassword())) {
            String id = session.getId();
            user.setId(new Random(10).nextLong());
            session.setAttribute("user", user);
            data.put("sessionId", id);
            data.put("user", user);
            return data;
        }
        return data;
    }

    @RequestMapping("/logout")
    public Object logout() {
        Enumeration<String> enumeration = session.getAttributeNames();
        while (enumeration.hasMoreElements()) {
            session.removeAttribute(enumeration.nextElement().toString());
        }
        session.removeAttribute("user");
        session.invalidate();
        return "success";
    }
}
