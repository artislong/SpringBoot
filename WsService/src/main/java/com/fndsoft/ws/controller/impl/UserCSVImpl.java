package com.fndsoft.ws.controller.impl;

import com.fndsoft.ws.commons.GlobleConstant;
import com.fndsoft.ws.controller.interfaces.IUserCSV;
import com.fndsoft.ws.wsBean.User;
import com.fndsoft.ws.wsService.interfaces.IUserSV;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@WebService(
        serviceName = "IUserCSV", portName = "IUserCSV",
        targetNamespace = GlobleConstant.TARGET_NAMESPACE,
        endpointInterface = "com.fndsoft.ws.controller.interfaces.IUserCSV"
)
@Service
public class UserCSVImpl implements IUserCSV {
    @Autowired
    private IUserSV userSV;
    @Override
    public User addUser(String username, String password) {
        return userSV.addUser(username, password);
    }
}
