package com.gnd.ws;

import com.alibaba.fastjson.JSON;
import com.gnd.ws.userCSV.User;
import com.gnd.ws.userCSV.UserSVImpl_Service;
import com.gnd.ws.userCSV.UserSVImpl_ServiceLocator;
import com.gnd.ws.weather.WeatherWebService;
import com.gnd.ws.weather.WeatherWebServiceLocator;
import com.gnd.ws.weather.WeatherWebServiceSoap_PortType;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceMessageExtractor;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.xml.rpc.ServiceException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WsClientApplicationTests {

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Test
    public void test01() {
        // http://www.webxml.com.cn/WebServices/WeatherWebService.asmx
        WeatherWebService service = new WeatherWebServiceLocator();
        try {
            WeatherWebServiceSoap_PortType weatherWebServiceSoap = service.getWeatherWebServiceSoap();
            String[] supportProvinces = weatherWebServiceSoap.getSupportProvince();
            for (String supportProvince : supportProvinces) {
                String[] supportCities = weatherWebServiceSoap.getSupportCity(supportProvince);
                System.out.println("省：" + supportProvince);
                for (String supportCity :supportCities){
                    System.out.println("\t -- 省市：" + supportCity);
                }
            }

            String[] shanghai = weatherWebServiceSoap.getWeatherbyCityName("上海");
            for (String sh : shanghai) {
                System.out.println(sh);
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test02() {
        UserSVImpl_Service usersv = new UserSVImpl_ServiceLocator();
        try {
            User user = usersv.getUserSVImpl().addUser("admin", "123");
            System.out.println(user);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test03(){
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx");
        try {
            Object[] objects = client.invoke("getSupportCity", "直辖市");
            System.out.println(JSON.toJSONString(objects[0]));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void test04() {
        Object o = webServiceTemplate.sendAndReceive("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx",
                new WebServiceMessageCallback() {
                    @Override
                    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
                        System.out.println(message);
                    }
                }, new WebServiceMessageExtractor<Object>() {
                    @Override
                    public Object extractData(WebServiceMessage message) throws IOException, TransformerException {
                        System.out.println(message);
                        return null;
                    }
                });
    }
}
