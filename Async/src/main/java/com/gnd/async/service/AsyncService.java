package com.gnd.async.service;

import com.gnd.async.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

@Service
public class AsyncService {
    private static final Logger logger = LoggerFactory.getLogger(AsyncService.class);
    public static Random random =new Random();

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public String test01() throws Exception {
        System.out.println("AsyncService.test01");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long end = System.currentTimeMillis();
        System.out.println("AsyncService.test01，耗时：" + (end - start) + "毫秒");
        return "ok1";
    }

    @Async
    public String test02() throws Exception {
        System.out.println("AsyncService.test02");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long end = System.currentTimeMillis();
        System.out.println("AsyncService.test02，耗时：" + (end - start) + "毫秒");
        return "ok2";
    }

    @Async
    public String test03() throws Exception {
        System.out.println("AsyncService.test03");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(10000));
        long end = System.currentTimeMillis();
        System.out.println("AsyncService.test03，耗时：" + (end - start) + "毫秒");
        return "ok3";
    }

    @Async
    public CompletableFuture<User> findUser(String userName) throws Exception {
        logger.info("Looking up" + userName);
        String url = String.format("https://api.github.com/users/%s", userName);
        User user = restTemplate.getForObject(url, User.class);
        Thread.sleep(1000);
        return CompletableFuture.completedFuture(user);
    }
}
