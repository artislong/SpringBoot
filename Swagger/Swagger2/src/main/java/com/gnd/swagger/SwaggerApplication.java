package com.gnd.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/8/5.
 */
@SpringBootApplication
public class SwaggerApplication {
    public static void main(String[] args){
        SpringApplication.run(SwaggerApplication.class, args);
    }
}
