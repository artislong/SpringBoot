package com.gnd.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by 陈敏 on 2017/7/3.
 */
@SpringBootApplication
public class RedisApplication {

    public static void main(String[] args){
        SpringApplication.run(RedisApplication.class, args);
    }
}
