package com.fndsoft.ws.wsBean;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@Data
public class OperationResult implements Serializable {
    private static final long serialVersionUID = -4337699954800131722L;

    private Boolean succeed;
    private String msg;

    public OperationResult() {
    }

    public OperationResult(Boolean succeed, String msg) {
        this.succeed = succeed;
        this.msg = msg;
    }
}
