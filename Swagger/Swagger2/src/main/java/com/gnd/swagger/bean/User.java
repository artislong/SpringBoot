package com.gnd.swagger.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Created by 陈敏 on 2017/8/5.
 */
@Data
public class User {
    private Long id;
    private String username;
    private String password;
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date date;
}
