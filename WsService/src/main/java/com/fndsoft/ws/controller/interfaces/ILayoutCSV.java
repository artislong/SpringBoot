package com.fndsoft.ws.controller.interfaces;

import com.fndsoft.ws.commons.GlobleConstant;
import com.fndsoft.ws.wsBean.OperationResult;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@WebService(name = "ILayoutCtrlSV", targetNamespace = GlobleConstant.TARGET_NAMESPACE)
public interface ILayoutCSV {

    @WebMethod
    String sayHello(@WebParam(name = "name") String name);

    @WebMethod
    OperationResult addLayout(@WebParam(name = "layoutName") String layoutName,
                              @WebParam(name = "layoutContent") String layoutContent);
}
