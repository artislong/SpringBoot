package com.gnd.jdbcsession.controller;

import com.gnd.jdbcsession.bean.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by 陈敏 on 2017/8/16.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private Map<String, Object> data = new HashMap<>();

    @RequestMapping("/login")
    public Object login(@RequestBody User user, HttpSession session) {
        if("admin".equals(user.getUsername()) && "admin".equals(user.getPassword())) {
            String id = session.getId();
            user.setId(new Random(10).nextLong());
            session.setAttribute("user", user);
            data.put("sessionId", id);
            data.put("user", user);
            return data;
        }
        return data;
    }

    @RequestMapping("/logout")
    public Object logout(HttpSession session) {
        // 将当前会话中的所有数据
        Enumeration<String> em = session.getAttributeNames();
        while (em.hasMoreElements()) {
            session.removeAttribute(em.nextElement().toString());
        }
        session.removeAttribute("user");
        session.invalidate();
        return "success";
    }
}
