package com.gnd.redis.test;

import com.alibaba.fastjson.JSON;
import com.gnd.redis.test.bean.Domain;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 陈敏 on 2017/7/3.
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisApplicationTests {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void test01() {

        List<String> oids = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Domain domain = new Domain();
            domain.setCode("haha" + i);
            domain.setName("gaga" + i);
            domain.setType("hehe" + i);
            domain.setOrderNum("heihei" + i);
            String oid = domain.getOid();
            oids.add(oid);
            System.out.println(i + "==" + oid);
            redisTemplate.opsForValue().set(oid, JSON.toJSONString(domain));
        }

        for (String oid : oids) {
            String jsonBean = redisTemplate.opsForValue().get(oid);
            Domain domain = JSON.parseObject(jsonBean, Domain.class);
            System.out.println(domain.toString());
        }
    }

    @Test
    public void test02() {
        for (int i = 0; i < 10; i++) {
            System.out.println(redisTemplate.opsForValue().get("name" + i));
        }
    }

    @Test
    public void test03() throws Exception {
        log.info("send message...");
        redisTemplate.convertAndSend("chat", "Hello");
    }
}
