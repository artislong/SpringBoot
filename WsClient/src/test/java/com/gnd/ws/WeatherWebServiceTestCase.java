/**
 * WeatherWebServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.gnd.ws;

import com.gnd.ws.weather.GetSupportDataSetResponseGetSupportDataSetResult;
import com.gnd.ws.weather.WeatherWebServiceLocator;
import com.gnd.ws.weather.WeatherWebServiceSoap_BindingStub;
import junit.framework.TestCase;

public class WeatherWebServiceTestCase extends TestCase {

    public void testWeatherWebServiceSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new WeatherWebServiceLocator().getWeatherWebServiceSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new WeatherWebServiceLocator().getServiceName());
    }

    public void test1WeatherWebServiceSoapGetSupportCity() throws Exception {
        WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (WeatherWebServiceSoap_BindingStub)
                          new WeatherWebServiceLocator().getWeatherWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getSupportCity(new java.lang.String());
        // TBD - validate results
    }

    public void test2WeatherWebServiceSoapGetSupportProvince() throws Exception {
        WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (WeatherWebServiceSoap_BindingStub)
                          new WeatherWebServiceLocator().getWeatherWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getSupportProvince();
        // TBD - validate results
    }

    public void test3WeatherWebServiceSoapGetSupportDataSet() throws Exception {
        WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (WeatherWebServiceSoap_BindingStub)
                          new WeatherWebServiceLocator().getWeatherWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        GetSupportDataSetResponseGetSupportDataSetResult value = null;
        value = binding.getSupportDataSet();
        // TBD - validate results
    }

    public void test4WeatherWebServiceSoapGetWeatherbyCityName() throws Exception {
        WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (WeatherWebServiceSoap_BindingStub)
                          new WeatherWebServiceLocator().getWeatherWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getWeatherbyCityName(new java.lang.String());
        // TBD - validate results
    }

    public void test5WeatherWebServiceSoapGetWeatherbyCityNamePro() throws Exception {
        WeatherWebServiceSoap_BindingStub binding;
        try {
            binding = (WeatherWebServiceSoap_BindingStub)
                          new WeatherWebServiceLocator().getWeatherWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getWeatherbyCityNamePro(new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

}
