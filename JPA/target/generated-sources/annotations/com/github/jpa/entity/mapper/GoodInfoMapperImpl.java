package com.github.jpa.entity.mapper;

import com.github.jpa.entity.GoodInfoBean;

import com.github.jpa.entity.GoodTypeBean;

import com.github.jpa.entity.dto.GoodInfoDTO;

import javax.annotation.Generated;

import org.springframework.stereotype.Component;

@Generated(

    value = "org.mapstruct.ap.MappingProcessor",

    date = "2018-02-23T14:39:25+0800",

    comments = "version: 1.2.0.CR1, compiler: javac, environment: Java 1.8.0_91 (Oracle Corporation)"

)

@Component

public class GoodInfoMapperImpl implements GoodInfoMapper {

    @Override

    public GoodInfoDTO from(GoodInfoBean good, GoodTypeBean type) {

        if ( good == null && type == null ) {

            return null;
        }

        GoodInfoDTO goodInfoDTO = new GoodInfoDTO();

        if ( good != null ) {

            if ( good.getId() != null ) {

                goodInfoDTO.setGoodId( String.valueOf( good.getId() ) );
            }

            goodInfoDTO.setGoodName( good.getTitle() );

            goodInfoDTO.setGoodPrice( good.getPrice() );
        }

        if ( type != null ) {

            goodInfoDTO.setTypeName( type.getName() );
        }

        return goodInfoDTO;
    }
}

