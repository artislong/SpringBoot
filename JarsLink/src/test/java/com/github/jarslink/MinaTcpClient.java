package com.github.jarslink;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
/**
 * @说明 Mina TCP客户端 
 * @author jacie.wang
 * @version 1.0 
 * @since 
 */  
public class MinaTcpClient extends IoHandlerAdapter { 
	private static Log log = LogFactory.getLog(MinaTcpClient.class);
    private IoConnector connector;  
    private static IoSession session;  
    public MinaTcpClient() {  
        connector = new NioSocketConnector();  
        connector.setHandler(this);
        /*ConnectFuture connFuture = connector.connect((new InetSocketAddress("localhost", MinaTcpServer.PORT)));*/
        ConnectFuture connFuture = null;
        String ip = null;
        int port = 0;
		try {
			ip = "";
			port = 8080;
			System.out.println("MinaTcpClient     ip: " + ip + "\r\n" + "port: " + port);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		connFuture = connector.connect(new InetSocketAddress(ip,port));
        connFuture.awaitUninterruptibly();  
        session = connFuture.getSession();  
        System.out.println("MinaTcpClient     TCP client started");  
    }  
    public static void main(String[] args) throws Exception {  
        MinaTcpClient client = new MinaTcpClient();  
            byte[] bts = "短信测试".getBytes();  
            IoBuffer buffer = IoBuffer.allocate(20);  
            // 自动扩容  
            buffer.setAutoExpand(true);  
            // 自动收缩  
            buffer.setAutoShrink(true);  
            buffer.put(bts);  
            buffer.flip();  
            session.write(buffer);  
            Thread.sleep(2000);  
        // 关闭会话，待所有线程处理结束后  
        client.connector.dispose(true);  
    }  
    @Override  
    public void messageReceived(IoSession iosession, Object message)  
            throws Exception {  
        IoBuffer bbuf = (IoBuffer) message;  
        byte[] byten = new byte[bbuf.limit()];  
        bbuf.get(byten, bbuf.position(), bbuf.limit());  
//        System.out.println("客户端收到消息" + ByteAndStr16.Bytes2HexString(byten));  
        System.out.println("MinaTcpClient     client get msg: " + new String(byten));  
    }  
    @Override  
    public void exceptionCaught(IoSession session, Throwable cause)  
            throws Exception {  
        System.out.println("MinaTcpClient     client exception");  
        super.exceptionCaught(session, cause);  
    }  
    @Override  
    public void messageSent(IoSession iosession, Object obj) throws Exception {  
        System.out.println("MinaTcpClient     client send msg");  
        super.messageSent(iosession, obj);  
    }  
    @Override  
    public void sessionClosed(IoSession iosession) throws Exception {  
        System.out.println("MinaTcpClient     client closed");  
        super.sessionClosed(iosession);  
    }  
    @Override  
    public void sessionCreated(IoSession iosession) throws Exception {  
        System.out.println("MinaTcpClient     client session created");  
        super.sessionCreated(iosession);  
    }  
    @Override  
    public void sessionIdle(IoSession iosession, IdleStatus idlestatus)  
            throws Exception {  
        System.out.println("MinaTcpClient     client slepping");  
        super.sessionIdle(iosession, idlestatus);  
    }  
    @Override  
    public void sessionOpened(IoSession iosession) throws Exception {  
        System.out.println("MinaTcpClient     client session opened");  
        super.sessionOpened(iosession);  
    }
	public void sendSM(String data) {
		MinaTcpClient client = new MinaTcpClient();  
		System.out.println(data);
//        byte[] bts = data.getBytes();  
        byte[] bts = null;
		try {
			bts = data.getBytes("utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}  
        IoBuffer buffer = IoBuffer.allocate(bts.length);  
        // 自动扩容  
        buffer.setAutoExpand(true);  
        // 自动收缩  
        buffer.setAutoShrink(true);  
        buffer.put(bts);  
        buffer.flip();  
        session.write(buffer);  
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}  
        // 关闭会话，待所有线程处理结束后  
        client.connector.dispose(true);
	}
}  