package com.gnd.redis.test.bean;

/**
 * 
  *<p>类描述：Entity通用父类。</p>
  * @author 张锡梓
  * @version v1.0.0.1。
  * @since JDK1.8。
  *<p>创建日期：2017年7月24日 上午11:40:28。</p>
 */
public abstract class CommonEntity extends BaseEntity{

}
