package com.fndsoft.ws;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by 陈敏 on 2017/7/20.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WsServiceApplicationTests {

}
